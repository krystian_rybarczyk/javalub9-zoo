package pl.sdacademy.animals.bear;


import org.joda.time.LocalDate;

public interface Clock {

    LocalDate getCurrentDate();
}
