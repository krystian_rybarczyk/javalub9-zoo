package pl.sdacademy.animals.bear;

import org.joda.time.LocalDate;
import pl.sdacademy.animals.Animal;



public abstract class Bear implements Animal {

    private int weight;
    private boolean isAlive;
    protected Clock clock;

    public Bear(int weight) {
        this.weight = weight;
        this.isAlive = false;
        this.clock = new ProductionClock();
    }

    public Bear(int weight, Clock clock) {
        this.weight = weight;
        this.isAlive = false;
        this.clock = clock;
    }

    @Override
    public boolean isAlive() {
        return isAlive;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    public void eat(int mealWeight) {
        this.isAlive = true;
        this.weight += mealWeight;
    }

    public abstract boolean isHibernating();
}
