package pl.sdacademy.animals.bear;

import org.joda.time.LocalDate;

public class BlackBear extends Bear {
    public BlackBear(int weight, Clock clock) {
        super(weight, clock);
    }

    public BlackBear(int weight) {
        super(weight);
    }

    @Override
    public boolean isHibernating() {
        LocalDate currentDate = clock.getCurrentDate();
        LocalDate november20th = currentDate
                .withMonthOfYear(11)
                .withDayOfMonth(20);
        LocalDate march15th = currentDate
                .withMonthOfYear(3)
                .withDayOfMonth(15);
        return currentDate.isAfter(november20th)
                || currentDate.isBefore(march15th);
    }
}