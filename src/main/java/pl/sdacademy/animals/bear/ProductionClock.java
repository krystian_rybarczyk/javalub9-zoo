package pl.sdacademy.animals.bear;


import org.joda.time.LocalDate;

public class ProductionClock implements Clock {

    @Override
    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }
}
