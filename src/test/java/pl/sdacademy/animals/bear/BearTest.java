package pl.sdacademy.animals.bear;

import org.joda.time.LocalDate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BearTest {

    @Test
    public void bearShouldNotBeAliveImmediatelyAfterCreation() {
        int weight = 3;
        Bear bear = new BlackBear(weight);

        boolean result = bear.isAlive();

        assertFalse(result);
    }

    @Test
    public void bearShouldBeAliveIfItHasEatenWithin10Days() {
        int weight = 3;
        Bear bear = new BlackBear(weight);
        int mealWeight = 0;
        bear.eat(mealWeight);

        boolean result = bear.isAlive();

        assertTrue(result);
    }

    @Test
    public void bearsWeightShouldIncreaseByMealsWeightAfterEating() {
        int birthWeight = 3;
        int mealWeight = 3;
        Bear bear = new BlackBear(birthWeight);
        bear.eat(mealWeight);

        int currentWeight = bear.getWeight();

        assertEquals(birthWeight + mealWeight, currentWeight);
    }

    @Test
    public void bearShouldBeAliveIfAndOnlyIfItsCurrentWeightIsNoLessThanItsBirthWeight() {
        int birthWeight = 3;
        Bear bear = new BlackBear(birthWeight);
        assert bear.getWeight() >= birthWeight;

        assertTrue(bear.isAlive());
    }

    @Test
    public void blackBearShouldBeHibernatingFrom20thNovemberUntil15thMarch() {
        int birthWeight = 3;
        Clock decemberClock = mock(Clock.class);
        when(decemberClock.getCurrentDate())
                .thenReturn(LocalDate.now()
                        .withMonthOfYear(12));
        Bear bear = new BlackBear(birthWeight, decemberClock);

        assertTrue(bear.isHibernating());
    }
}
